1、线程池

1.1 简介

1.2 使用pthread实现简单的线程池



2、OpenCL调用GPU加速

2.1 OpenCL使用步骤

​		OpenCL使用起来还是比较简单的，流程如下图。

![1](D:\文档\htq_library\多线程NPU\1.png)

在编程中，需要先获得Device、设置Context，创建Comand Queue和内存对象，之后将OpenCL写的核函数读取到程序中，对核函数进行编译，最后将内存中的数据copy到显存，执行核函数代码，将结果从显存copy到内存。

这里附一个简单的OpenCL代码。

```
opencl  code



```

OpenCL的语法接近C语言，如果对OpenCL不熟悉，需要相关学习资料的请百度，博主在最后也放了一个OpenCL的教程链接。

2.2 RK3588S使用OpenCL调用GPU加速

​		RK3588S使用OpenCL调用GPU加速代码执行，博主以前写过这样的文章：https://blog.csdn.net/zichuanning520/article/details/129411886。详细的请参考该篇文章，这里只是将使用方法简单的说下。

​		在Makefile中添加OpenCL的库和路径。

```makefile
OPENCL_LDLIBS = -lmali
OPENCL_LDLIBS_PATH = -L/usr/lib/aarch64-linux-gnu
```

