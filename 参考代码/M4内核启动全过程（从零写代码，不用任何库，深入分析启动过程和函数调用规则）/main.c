
#include "spc1168.h"
#include "uart.h"

void SystemHardWareInit(void);
int a = 0x10;
//int a = 10;
//int b[10] = {0};
//int C[] = {1,2,3,4};
//typedef struct section_info
//{
//    uint32_t base;
//    uint32_t limit;
//    const char* name;   //��ʾ�ö�����
//    
//}section_info_t;


//void print_section_info(section_info_t* si)
//{
////    printf("%s\n", si->name);
////    printf("    base:   0x%08x\n", si->base);
////    printf("    limit:  0x%08x\n", si->limit);
////    printf("    length: %d\n", si->limit - si->base);
//}

extern int Region$$Table$$Base;



extern int Image$$ER_IROM1$$Base;
extern int Image$$ER_IROM1$$Limit;
extern int Image$$ER_IROM1$$RO$$Base;
extern int Image$$ER_IROM1$$RO$$Limit;
extern int Image$$ER_IROM1$$RW$$Base; 
extern int Image$$ER_IROM1$$RW$$Limit;
extern int Image$$ER_IROM1$$ZI$$Base;
extern int Image$$ER_IROM1$$ZI$$Limit;



extern int Load$$ER_IROM1$$Base;
extern int Load$$ER_IROM1$$Limit;
extern int Load$$ER_IROM1$$RO$$Base;
extern int Load$$ER_IROM1$$RO$$Limit;
extern int Load$$ER_IROM1$$RW$$Base; 
extern int Load$$ER_IROM1$$RW$$Limit;
extern int Load$$ER_IROM1$$ZI$$Base;
extern int Load$$ER_IROM1$$ZI$$Limit;



extern int Image$$RW_IRAM1$$Base;
extern int Image$$RW_IRAM1$$Limit;
extern int Image$$RW_IRAM1$$RO$$Base;
extern int Image$$RW_IRAM1$$RO$$Limit;
extern int Image$$RW_IRAM1$$RW$$Base; 
extern int Image$$RW_IRAM1$$RW$$Limit;
extern int Image$$RW_IRAM1$$ZI$$Base;
extern int Image$$RW_IRAM1$$ZI$$Limit;

extern int Load$$RW_IRAM1$$Base;
extern int Load$$RW_IRAM1$$Limit;
extern int Load$$RW_IRAM1$$RO$$Base;
extern int Load$$RW_IRAM1$$RO$$Limit;
extern int Load$$RW_IRAM1$$RW$$Base; 
extern int Load$$RW_IRAM1$$RW$$Limit;
extern int Load$$RW_IRAM1$$ZI$$Base;
extern int Load$$RW_IRAM1$$ZI$$Limit;



//image code + ro
extern int Image$$ER_IROM1$$Base;
extern int Image$$ER_IROM1$$Limit;

//image rw
extern int Image$$RW_IRAM1$$RW$$Base;
extern int Image$$RW_IRAM1$$RW$$Limit;

//load rw
extern int Load$$RW_IRAM1$$RW$$Base;
extern int Load$$RW_IRAM1$$RW$$Limit;

//image zi
extern int Image$$RW_IRAM1$$ZI$$Base;
extern int Image$$RW_IRAM1$$ZI$$Limit;


void rw_section_init(uint32_t src_addr, uint32_t dst_addr, uint32_t length)
{
    uint32_t * psrc, *pdst, *pend;
    
    psrc = (uint32_t*)src_addr;
    pdst = (uint32_t*)dst_addr;
    pend = (uint32_t*)(src_addr + length);
    
    while(psrc < pend){
        *pdst++ = *psrc++;
    }
}

void zi_section_init(uint32_t src_addr, uint32_t length)
{
    uint8_t * psrc, *pend;
    
    psrc = (uint8_t*)src_addr;
    pend = (uint8_t*)(src_addr + length - 16); 
    while(psrc < pend){
        *psrc++ = 0;
    }
    //send_str("clear zi\r\n");
}


int main()
{

    uint32_t image_rw_start_addr = (uint32_t)&Image$$RW_IRAM1$$RW$$Base;  //0x20000000
    uint32_t image_rw_length = (uint32_t)&Image$$RW_IRAM1$$RW$$Limit - (uint32_t)&Image$$RW_IRAM1$$RW$$Base; //0x00000004
    
    uint32_t load_rw_start_addr = (uint32_t)&Load$$RW_IRAM1$$RW$$Base;  //0x10000B20

    uint32_t image_zi_start_addr = (uint32_t)&Image$$RW_IRAM1$$ZI$$Base; //0x20000004
    uint32_t image_zi_length = (uint32_t)&Image$$RW_IRAM1$$ZI$$Limit - (uint32_t)&Image$$RW_IRAM1$$ZI$$Base; //0x00000114
    
    rw_section_init(load_rw_start_addr, image_rw_start_addr, image_rw_length); //0x10000B20, 0x20000000, 0x00000004
    SystemHardWareInit();
    zi_section_init(image_zi_start_addr, image_zi_length);

    send_str("123\r\n");

    uint8_t *pstart = (uint8_t*)&a, *pend;
    uint8_t *p;
    uint8_t c = 0;
    pstart = (uint8_t *)image_zi_start_addr;
    pend = (uint8_t *)(image_zi_start_addr + image_zi_length);
    for(p = pstart; p < pend;p++)
    {
        if(*p != '\0')
        {
            c = '0';
            send_ch(c);
        }
        else{
            send_str("1");
        }        
    
    }
    
    while(1);
    
    
//    
//    section_info_t  image_rom_ro = {(uint32_t)&Image$$ER_IROM1$$RO$$Base, (uint32_t)&Image$$ER_IROM1$$RO$$Limit, "IMAGE_ROM_RO"};
//    section_info_t  image_ram_rw = {(uint32_t)&Image$$RW_IRAM1$$RW$$Base, (uint32_t)&Image$$RW_IRAM1$$RW$$Limit, "IMAGE_RAM_RW"};
//    section_info_t  image_ram_zi = {(uint32_t)&Image$$RW_IRAM1$$ZI$$Base, (uint32_t)&Image$$RW_IRAM1$$ZI$$Limit, "IMAGE_RAM_ZI"};
//    
//    print_section_info(&image_rom_ro);
//    print_section_info(&image_ram_rw);
//    print_section_info(&image_ram_zi);
    
        
//    printf("ro    base:    0x%08x\n", image_ro_start_addr);
//    printf("ro    length:  0x%08d\n", image_ro_length);
//    
//    printf("rw    base:    0x%08x\n", image_rw_start_addr);
//    printf("rw    length:  0x%08d\n", image_rw_length);
//    
//    printf("rw    base:    0x%08x\n", load_rw_start_addr);
//    printf("rw    length:  0x%08d\n", load_rw_length);
//    
//    printf("zi    base:    0x%08x\n", image_zi_start_addr);
//    printf("zi    length:  0x%08d\n", image_zi_length);
    
    
    
    
    
    
//extern int Image$$ER_IROM1$$Limit;   
//extern int Image$$ER_IROM1$$RO$$Limit;
//extern int Image$$ER_IROM1$$RW$$Limit;
//extern int Image$$ER_IROM1$$ZI$$Limit;
//extern int Image$$ER_IROM1$$Length;

//    
//extern int Load$$ER_IROM1$$Base;
//extern int Load$$ER_IROM1$$RO$$Base;
//extern int Load$$ER_IROM1$$RW$$Base;
//extern uint32_t Load$$ER_IROM1$$ZI$$Base;


     
//extern int Load$$ER_IROM1$$RO$$Base;
//extern int Load$$ER_IROM1$$RW$$Base;   
//extern int Load$$ER_IROM1$$ZI$$Base; 


//extern int Load$$ER_IROM1$$Limit;
    
//    Image$$ER_IROM1$$Base;
//    Image$$ER_IROM1$$Limit;
//    Image$$ER_IROM1$$Length;

//    section_info_t  image_rom =    {(uint32_t)&Image$$ER_IROM1$$Base, (uint32_t)&Image$$ER_IROM1$$Limit, "IMAGE_ROM"};
//    section_info_t  image_rom_ro = {(uint32_t)&Image$$ER_IROM1$$RO$$Base, (uint32_t)&Image$$ER_IROM1$$RO$$Limit, "IMAGE_ROM_RO"};
//    section_info_t  image_rom_rw = {(uint32_t)&Image$$ER_IROM1$$RW$$Base, (uint32_t)&Image$$ER_IROM1$$RW$$Limit, "IMAGE_ROM_RW"};
//    section_info_t  image_rom_zi = {(uint32_t)&Image$$ER_IROM1$$ZI$$Base, (uint32_t)&Image$$ER_IROM1$$ZI$$Limit, "IMAGE_ROM_ZI"};
//    
//    section_info_t  load_rom =    {(uint32_t)&Load$$ER_IROM1$$Base, (uint32_t)&Load$$ER_IROM1$$Limit, "LOAD_ROM"};
//    section_info_t  load_rom_ro = {(uint32_t)&Load$$ER_IROM1$$RO$$Base, (uint32_t)&Load$$ER_IROM1$$RO$$Limit, "LOAD_ROM_RO"};
//    section_info_t  load_rom_rw = {(uint32_t)&Load$$ER_IROM1$$RW$$Base, (uint32_t)&Load$$ER_IROM1$$RW$$Limit, "LOAD_ROM_RW"};
//    section_info_t  load_rom_zi = {(uint32_t)&Load$$ER_IROM1$$ZI$$Base, (uint32_t)&Load$$ER_IROM1$$ZI$$Limit, "LOAD_ROM_ZI"};
//    
//    
//    section_info_t  image_ram =    {(uint32_t)&Image$$RW_IRAM1$$Base, (uint32_t)&Image$$RW_IRAM1$$Limit, "IMAGE_RAM"};
//    section_info_t  image_ram_ro = {(uint32_t)&Image$$RW_IRAM1$$RO$$Base, (uint32_t)&Image$$RW_IRAM1$$RO$$Limit, "IMAGE_RAM_RO"};
//    section_info_t  image_ram_rw = {(uint32_t)&Image$$RW_IRAM1$$RW$$Base, (uint32_t)&Image$$RW_IRAM1$$RW$$Limit, "IMAGE_RAM_RW"};
//    section_info_t  image_ram_zi = {(uint32_t)&Image$$RW_IRAM1$$ZI$$Base, (uint32_t)&Image$$RW_IRAM1$$ZI$$Limit, "IMAGE_RAM_ZI"};
//    
//    section_info_t  load_ram =    {(uint32_t)&Load$$RW_IRAM1$$Base, (uint32_t)&Load$$RW_IRAM1$$Limit, "LOAD_RAM"};
//    section_info_t  load_ram_ro = {(uint32_t)&Load$$RW_IRAM1$$RO$$Base, (uint32_t)&Load$$RW_IRAM1$$RO$$Limit, "LOAD_RAM_RO"};
//    section_info_t  load_ram_rw = {(uint32_t)&Load$$RW_IRAM1$$RW$$Base, (uint32_t)&Load$$RW_IRAM1$$RW$$Limit, "LOAD_RAM_RW"};
//    section_info_t  load_ram_zi = {(uint32_t)&Load$$RW_IRAM1$$ZI$$Base, (uint32_t)&Load$$RW_IRAM1$$ZI$$Limit, "LOAD_RAM_ZI"};
//    
//    
//    b[0] = 1;
//    a = 10;
//    print_section_info(&image_rom);
//    print_section_info(&image_rom_ro);
//    print_section_info(&image_rom_rw);
//    print_section_info(&image_rom_zi);
//    
//    
//    print_section_info(&load_rom);
//    print_section_info(&load_rom_ro);
//    print_section_info(&load_rom_rw);
//    print_section_info(&load_rom_zi);
//    
//    
//    
//    print_section_info(&image_ram);
//    print_section_info(&image_ram_ro);
//    print_section_info(&image_ram_rw);
//    print_section_info(&image_ram_zi);
//    
//    
//    print_section_info(&load_ram);
//    print_section_info(&load_ram_ro);
//    print_section_info(&load_ram_rw);
//    print_section_info(&load_ram_zi);
//    
    
    //
        
//    printf("ER_IROM1_base:      0x%08x\n", ER_IROM1_base);
//    printf("ER_IROM1_length:     0x%08x\n", ER_IROM1_length);
//    printf("len: %d\n", Image$$ER_IROM1$$Base - Image$$ER_IROM1$$Limit);
//    printf("\n");
//    printf("Image$$ER_IROM1$$RO$$Base:     0x%08x\n", Image$$ER_IROM1$$RO$$Base);
//    printf("Image$$ER_IROM1$$RO$$Limit:    0x%08x\n", Image$$ER_IROM1$$RO$$Limit);
//    printf("\n");
//    printf("Image$$ER_IROM1$$RW$$Base:     0x%08x\n", Image$$ER_IROM1$$RW$$Base);
//    printf("Image$$ER_IROM1$$RW$$Limit:    0x%08x\n", Image$$ER_IROM1$$RW$$Limit);
//    printf("\n");
//    printf("Image$$ER_IROM1$$ZI$$Base:     0x%08x\n", Image$$ER_IROM1$$ZI$$Base);
//    printf("Image$$ER_IROM1$$ZI$$Limit:    0x%08x\n", Image$$ER_IROM1$$ZI$$Limit);




//    printf("Load$$ER_IROM1$$Base:      0x%08x\n", Load$$ER_IROM1$$Base);
//    printf("Load$$ER_IROM1$$Limit:     0x%08x\n", Load$$ER_IROM1$$Limit);
//    printf("\n");
//    printf("Load$$ER_IROM1$$RO$$Base:     0x%08x\n", Load$$ER_IROM1$$RO$$Base);
//    printf("Load$$ER_IROM1$$RO$$Limit:    0x%08x\n", Load$$ER_IROM1$$RO$$Limit);
//    printf("\n");
//    printf("Load$$ER_IROM1$$RW$$Base:     0x%08x\n", Load$$ER_IROM1$$RW$$Base);
//    printf("Load$$ER_IROM1$$RW$$Limit:    0x%08x\n", Load$$ER_IROM1$$RW$$Limit);
//    printf("\n");
//    printf("Load$$ER_IROM1$$ZI$$Base:     0x%08x\n", Load$$ER_IROM1$$ZI$$Base);
//    printf("Load$$ER_IROM1$$ZI$$Limit:    0x%08x\n", Load$$ER_IROM1$$ZI$$Limit);
    
    
    
//    printf("Image$$ER_IROM1$$ZI$$Base:     0x%08x\n", Image$$ER_IROM1$$ZI$$Limit);
////    printf("Image$$ER_IROM1$$Length:     0x%08x\n", Image$$ER_IROM1$$Length);
//    
//    printf("Load$$ER_IROM1$$RO$$Base:     0x%08x\n", Load$$ER_IROM1$$RO$$Base);
//    printf("Load$$ER_IROM1$$RW$$Base:     0x%08x\n", Load$$ER_IROM1$$RW$$Base);
//    printf("Load$$ER_IROM1$$ZI$$Base:     0x%08x\n", Load$$ER_IROM1$$ZI$$Base);
//    
////    printf("Image$$ER_IROM1$$CODE$$Base:     0x%08x\n", Image$$ER_IROM1$$CODE$$Base);
//    
////    printf("Image$$ER_IROM1$$Base:     0x%08x\n", Image$$ER_IROM1$$Base);
////    printf("Image$$ER_IROM1$$Limit:    0x%08x\n", Image$$ER_IROM1$$Limit);

////    printf("Load$$ER_IROM1$$Base:      0x%08x\n", Load$$ER_IROM1$$Base);
////    printf("Load$$ER_IROM1$$Limit:     0x%08x\n", Load$$ER_IROM1$$Limit);

    while(1);

}



void SystemHardWareInit(void)
{
    FLASH_WALLOW(FLASH);
    FLASH_SetTiming(200000000);
    FLASH_WDIS();
    CLOCK_InitWithRCO(CLOCK_HCLK_200MHZ);
    GPIO_SetPinChannel(GPIO_34, GPIO34_UART_TXD);
    GPIO_SetPinChannel(GPIO_35, GPIO35_UART_RXD);
    CLOCK_EnableModule(UART_MODULE);
    UART_Init(UART, 38400);
}

